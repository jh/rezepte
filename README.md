# Jacks Rezepte

Meine persönlichen Rezepte in Org-Mode


## Lizenz
Alle Dokumente in dieser Sammlung sind unter der [Creative Commons Attribution 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/) lizensiert.
Das Material darf unter Angabe des Urhebers und eventueller Änderungen frei geteilt, modifiziert und weiterverbreitet werden.

[![CC BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)
